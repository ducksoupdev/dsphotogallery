<?php
// DSPhotoGalleryHolder

class DSPhotoGalleryHolder extends Page {

    private static $db = array(
		//     Align title - select: left, center, right
		"ThumbnailAlignTitle" => "Varchar(16)",
		//     Filter effect - select: None, Shadow, Greyscale, Greyscale and Shadow
    	"ThumbnailFilterEffect" => "Varchar(64)"
    );

    private static $has_one = array(
    );
    
    private static $singular_name = "Photo gallery holder";
    
    private static $plural_name = "Photo gallery holders";
    
    private static $allowed_children = array('DSPhotoGalleryPage');
    
    private static $icon = "dsphotogallery/images/icons/dsphotogalleryholder";
	
	public function getCMSFields() {
    	$fields = parent::getCMSFields();

    	//     Align title - select: left, center, right
    	$fields->addFieldToTab("Root.Galleries", new DropdownField('ThumbnailAlignTitle', 'Align title', array('left' => 'Left', 'center' => 'Center', 'right' => 'Right'), "center"));
    	//     Filter effect - select: None, Shadow, Greyscale, Greyscale and Shadow
    	$fields->addFieldToTab("Root.Galleries", new DropdownField('ThumbnailFilterEffect', 'Filter effect', array('none' => 'None', 'shadow' => 'Shadow', 'greyscale' => 'Greyscale', 'shadow greyscale' => 'Greyscale and shadow'), "none"));
    	
		return $fields;
	}

}
  
class DSPhotoGalleryHolder_Controller extends Page_Controller {
	
	private static $allowed_actions = array (
    );
 
    public function init() {
    	parent::init();
    }
	
}
