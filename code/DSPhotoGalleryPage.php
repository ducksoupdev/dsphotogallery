<?php
// DSPhotoGalleryPage

class DSPhotoGalleryPage extends Page {

    public static $db = array(
    	// General tab:
    	//     Show resource name as title - boolean
    	"ShowResourceNameAsTitle" => "Boolean",
    	
    	// Thumbnails tab:
    	//     Show title - boolean
    	"ThumbnailShowTitle" => "Boolean",
    	//     Align title - select: left, center, right
    	"ThumbnailAlignTitle" => "Varchar(16)",
    	//     Filter effect - select: None, Shadow, Greyscale, Greyscale and Shadow
    	"ThumbnailFilterEffect" => "Varchar(64)",
    	//     Top text - HTMLText
    	"ThumbnailTopText" => "HTMLText",
    	//     Bottom text - HTMLText
    	"ThumbnailBottomText" => "HTMLText",
    	
    	// Photo tab:
    	//     Show title - boolean
    	"PhotoShowTitle" => "Boolean",
    	//     Align title, description - select: left, center, right
    	"PhotoAlignTitle" => "Varchar(16)",
    	//     Show link to original - boolean
    	"PhotoShowLinkToOriginal" => "Boolean",
    	//     Open in
		"PhotoOpenIn" => "varchar(32)"
    );

    public static $has_one = array(
    	"GalleryFolder" => "Folder",
		"PhotoImage" => "Image"
    );
    
    public static $singular_name = "Photo gallery page";
    
    public static $plural_name = "Photo gallery pages";
    
    public static $icon = "dsphotogallery/images/icons/dsphotogallerypage";
    
    public function getCMSFields() {
    	$fields = parent::getCMSFields();
    	
    	// General tab:
		// gallery folder
		$fields->addFieldToTab("Root.Gallery", new TreeDropdownField("GalleryFolderID", "Gallery Folder", 'Folder'));
    	//     Show resource name as title - boolean
    	$fields->addFieldToTab("Root.Gallery", $generalTitle = new FieldGroup(new CheckboxField("ShowResourceNameAsTitle", "Show image name as title")));
    	$generalTitle->setTitle("Title");
		// add Image
		$uploadField = new UploadField('PhotoImage', 'Image');
		$uploadField->getValidator()->setAllowedExtensions(array('jpg', "JPG", 'jpeg', "JPEG", 'png', "PNG", 'gif', "GIF"));
        $fields->addFieldToTab('Root.Gallery', $uploadField);
    	
    	// Thumbnails tab:
    	//     Show title - boolean
    	$fields->addFieldToTab("Root.Thumbnails", $thumbnailsTitle = new FieldGroup(new CheckboxField("ThumbnailShowTitle", "Show title")));
    	$thumbnailsTitle->setTitle("Title");
    	//     Align title - select: left, center, right
    	$fields->addFieldToTab("Root.Thumbnails", new DropdownField('ThumbnailAlignTitle', 'Align title', array('left' => 'Left', 'center' => 'Center', 'right' => 'Right'), "center"));
    	//     Filter effect - select: None, Shadow, Greyscale, Greyscale and Shadow
    	$fields->addFieldToTab("Root.Thumbnails", new DropdownField('ThumbnailFilterEffect', 'Filter effect', array('none' => 'None', 'shadow' => 'Shadow', 'greyscale' => 'Greyscale', 'shadow greyscale' => 'Greyscale and shadow'), "none"));
    	//     Top text - HTMLText
    	$fields->addFieldToTab("Root.Thumbnails", $htmlToptext = new HTMLEditorField('ThumbnailTopText', 'Top text'));
    	$htmlToptext->addExtraClass('stacked');
    	//     Bottom text - HTMLText
    	$fields->addFieldToTab("Root.Thumbnails", $htmlBottomtext = new HTMLEditorField('ThumbnailBottomText', 'Bottom text'));
    	$htmlBottomtext->addExtraClass('stacked');
    	
    	// Photo tab:
    	//     Show title - boolean
    	$fields->addFieldToTab("Root.Photo", $photoTitle = new FieldGroup(new CheckboxField("PhotoShowTitle", "Show title")));
    	$photoTitle->setTitle("Title");
    	//     Align title, description - select: left, center, right
    	$fields->addFieldToTab("Root.Photo", new DropdownField('PhotoAlignTitle', 'Align title', array('left' => 'Left', 'center' => 'Center', 'right' => 'Right'), "center"));
    	//     Show link to original - boolean
    	$fields->addFieldToTab("Root.Photo", $showLink = new FieldGroup(new CheckboxField("PhotoShowLinkToOriginal", "Show link to original")));
    	$showLink->setTitle("Link");
    	//     Open in - select: Same window, New window, Pop-up window, Lightbox
    	$fields->addFieldToTab("Root.Photo", new DropdownField('PhotoOpenIn', 'Open in', array('lightbox' => 'Lightbox', '' => 'Same window', '_blank' => 'New window', 'popup' => 'Pop-up window'), "lightbox"));
    	
		// remove the content field
		$fields->removeByName('Content', true);
		
    	return $fields;
    }
	
	public function GalleryImages() {
		if ($this->GalleryFolderID) {
			return DataObject::get("Image", "ParentID = {$this->GalleryFolderID}");
		} 
	}

}
  
class DSPhotoGalleryPage_Controller extends Page_Controller {
	
	public static $allowed_actions = array (
    );
 
    public function init() {
    	parent::init();
    }
    
    public function photo(SS_HTTPRequest $request) {
    	// grab the ID of this photo
    	$photoID = $request->param("ID");
    	if ($photoID) {
    		$title = $this->Title;
    		$photoIndex = -1;
    		$currentIndex = 0;
    		$currentPhoto = null;
    		$allPhotos = DataObject::get("Image", "ParentID = {$this->GalleryFolderID}");
    		$allPhotosArray = $allPhotos->toArray();
    		foreach ($allPhotosArray as $photo) {
    			if ($photo->ID == $photoID) {
    				$photoIndex = $currentIndex;
    				$currentPhoto = $photo;
    				SS_Log::log("Photo found at index: " . $photoIndex, SS_Log::NOTICE);
    			}
    			$currentIndex++;
    		}
    		
    		// get all the photos and decide what is the next and previous ones
    		$nextIndex = $photoIndex + 1;
    		$prevIndex = $photoIndex - 1;
    		if ($nextIndex == count($allPhotosArray)) $nextIndex = 0;
    		if ($prevIndex == -1) $prevIndex = count($allPhotosArray) - 1;
    		$nextPhoto = $allPhotosArray[$nextIndex];
    		$prevPhoto = $allPhotosArray[$prevIndex];
    		
    		SS_Log::log("Current photo: " . print_r($currentPhoto, true), SS_Log::NOTICE);
    		SS_Log::log("Next photo: " . print_r($nextPhoto, true), SS_Log::NOTICE);
    		SS_Log::log("Prev photo: " . print_r($prevPhoto, true), SS_Log::NOTICE);
    		
    		return $this->customise(array(
    		        "Title" => $title,
    		        "CurrentPhoto" => $currentPhoto,
    		        "NextPhoto" => $nextPhoto,
    		        "PrevPhoto" => $prevPhoto
    		));
    	}
    }
	
}
