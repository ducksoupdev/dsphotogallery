$(document).ready(function() {
	if (typeof photoOpenIn != "undefined") {
		if (photoOpenIn == "lightbox") {
			$('#thumbs a.thumb').lightBox();
		} else if (photoOpenIn == "popup") {
			$('#thumbs a').on('click', function() { 
				window.open('Photo', $(this).attr('href'), 'width=640,height=480'); 
			});
		}
	}
});